'use strict';
let botonstart = document.getElementById('botonstart');
botonstart.addEventListener('click', iniciarpartida);
let selecciondecasilla = document.getElementById('seleccionarcasilla');
selecciondecasilla.addEventListener('keyup', verificarcarta);
let distribuciontablero = []; //arreglo que almacena la distrubicion del tablero
let arreglocartas = []; //almacena las img de las 8 cartas
/*cantidad de cada tipo de carta para la partida actual*/
let cantidadmarcas = 0;
let cantidadvacios = 0;
let cantidadbombas = 0;
/*cantidad de aciertos ,errores y partidas que se van jugando*/
let aciertos = 0;
let errores = 0;
let explosiones = 0;
/*variables globales a todas las partidas, estas nunca se reinician*/
let numeropartida = 0;
let aciertosglobales = 0;
let erroresglobales = 0;
let explosionesglobales = 0;

function iniciarpartida() {
  toggleinputcasilla(); //reinicia los contadores y llama a la funcion que arma el tablero de juego
  aciertos = 0;
  errores = 0;
  cantidadmarcas = 0;
  cantidadvacios = 0;
  cantidadbombas = 0;
  explosiones = 0;
  let stringtiempo = document.getElementById('tiempo').value;
  let tiempo = parseInt(stringtiempo);
  let distribuciontablero = armartablero(); //llama a la funcion que arma el tablero de juego
  if ((tiempo < 13) && (tiempo > 1)) { //se fija si el jugador selecciono algun tiempo diferente a 3 que es el valor por defecto
    let timer = setTimeout('taparcartas()', (tiempo * 1000));
    let tiempotogglecasilla=setTimeout('toggleinputcasilla()', (tiempo * 1000));
    alert('Tiene ' + tiempo + ' segundos para memorizar el tablero.')
  } else {
    let timer = setTimeout('taparcartas()', 3000);
    let tiempotogglecasilla=setTimeout('toggleinputcasilla()',3000);
    alert('Tiene 3 segundos para memorizar el tablero.')

  }
  let parciales = document.querySelectorAll('.resultadoparcial'); // estas y las dos siguientes: reinician el contador parcial para la partida actual
  parciales[0].innerHTML = 0;
  parciales[1].innerHTML = 0;
  if (cantidadmarcas === 0) { //condicional para ganar la partida si no hay marcas en el tablero
    numeropartida++;
    ganarjuego();
    let cartelganar = setTimeout('ganarjuego()', 3000);
    let timer = setTimeout('taparcartas()', 3000);
    agregarresultado();
    distribuciontablero = [];
  }
  return distribuciontablero;
}

function armartablero() { //funcion que genera el tablero de juego
  arreglocartas = document.querySelectorAll('.carta');
  for (let i = 0; i < arreglocartas.length; i++) { //llama a tirar tablero para cada una de las 8 cartas
    let carta = tirarcarta();
    if (carta <= 5) { //probabilidad de 0.5 de generar una carta con una marca
      arreglocartas[i].src = 'imagenes/marca.jpg';
      distribuciontablero[i] = 'marca';
      cantidadmarcas++;
    } else if (carta <= 9) { //probabilidad de 0.4 de generar una carta vacia
      arreglocartas[i].src = 'imagenes/vacio.jpg';
      distribuciontablero[i] = 'vacio';
      cantidadvacios++;
    } else { //probabilidad de 0.1 de generar una carta con una bomba
      arreglocartas[i].src = 'imagenes/bomba.jpg';
      distribuciontablero[i] = 'bomba';
      cantidadbombas++;
    }
  }
  let dificultad = parseInt(document.getElementById('dificultad').value);
  if (dificultad > 50) {
    if (distribuciontablero[0] != 'marca') {
      arreglocartas[0].src = 'imagenes/marca.jpg';
      distribuciontablero[0] = 'marca';
      cantidadmarcas++;
    }
  }
}

function tirarcarta() { //da un valor de 1 a 10 para designar un tipo de carta
  let probabilidad = Math.floor(10 * Math.random() + 1);
  return probabilidad;
}

function taparcartas() { //pone todas las cartas boca abajo
  for (let i = 0; i < arreglocartas.length; i++) {
    arreglocartas[i].src = 'imagenes/tapa.jpg';
  }
}

function toggleinputcasilla(){
  selecciondecasilla.classList.toggle('nomostrar');
}

function verificarcarta() { //se fija que carta eligio el usuario y ve si es marca vacia o bomba
  let casillaelegida = document.getElementById('seleccionarcasilla').value;
  document.getElementById('seleccionarcasilla').value = '';
  let numerocasilla = parseInt(casillaelegida);
  let possdelarreglo = numerocasilla - 1;
  if (distribuciontablero[possdelarreglo] === 'marca') { //si es una marca:la muestra y cuenta un acierto
    arreglocartas[possdelarreglo].src = 'imagenes/marca.jpg';
    cantidadmarcas--;
    aciertos++;
    aciertosglobales++;
    distribuciontablero[possdelarreglo] = 0; //borra el valor del arreglo para que esa carta no pueda volver a utilizarse
    if (cantidadmarcas === 0) { //se gana la partida al encontrar todas las marcas
      numeropartida++;
      ganarjuego(); //aparece el cartel de ganar
      let cartelganar = setTimeout('ganarjuego()', 3000); //desaparece el cartel de ganar
      let timer = setTimeout('taparcartas()', 3000);
      agregarresultado(); //agrega la fila de resultados para esta partida
      distribuciontablero = []; //reinicia al arreglo que contiene la distrubucion del tablero
    }
  } else if (distribuciontablero[possdelarreglo] === 'vacio') {
    arreglocartas[possdelarreglo].src = 'imagenes/vacio.jpg';
    errores++;
    erroresglobales++;
    distribuciontablero[possdelarreglo] = 0; //borra el valor del arreglo para que esa carta no pueda volver a utilizarse
  } else if (distribuciontablero[possdelarreglo] === 'bomba') {
    arreglocartas[possdelarreglo].src = 'imagenes/bomba.jpg';
    numeropartida++;
    explosiones++;
    explosionesglobales++;
    perderjuego(); //aparece el cartel de perder
    let cartelperder = setTimeout('perderjuego()', 3000); //desaparece el cartel de perder
    let timer = setTimeout('taparcartas()', 3000);
    agregarresultado(); //agrega la fila de resultados para esta partida
    distribuciontablero = []; //reinicia al arreglo que contiene la distrubucion del tablero
  } else if ((possdelarreglo > 7) || (possdelarreglo < 0)) {
    alert('ingrese del 1 al 8 por favor')
  } else {
    alert('la casilla  seleccionada no es valida o ya fue seleccionada')
  }
  /*luego de cada acierto o error se le muestra al jugador el resultado parcial de la partida
  y la suma de las partidas anteriores*/
  let parciales = document.querySelectorAll('.resultadoparcial');
  parciales[0].innerHTML = aciertos;
  parciales[1].innerHTML = errores;
  parciales[2].innerHTML = aciertosglobales;
  parciales[3].innerHTML = erroresglobales;
  parciales[4].innerHTML = explosionesglobales;
}

function ganarjuego() { //hace aparecer o desaparecer el mensaje de juego ganado
  let ganaste = document.querySelector('.mensajejuegoganado');
  ganaste.classList.toggle('nomostrar');
}

function perderjuego() { //hace aparecer o desaparecer el mensaje de juego ganado
  let perdiste = document.querySelector('.mensajejuegoperdido');
  perdiste.classList.toggle('nomostrar');
}

function agregarresultado() { //añade una fila a la tabla de resultados totales con los de esta partida
  let tabla = document.getElementById('resultadostotales');
  let row = tabla.insertRow();
  let cell0 = row.insertCell(0);
  let cell1 = row.insertCell(1);
  cell0.innerHTML = numeropartida;
  cell1.innerHTML = aciertos + '/' + errores + '/' + explosiones;
}
